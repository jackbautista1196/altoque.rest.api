<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");


$host = '134.209.163.214';
$user = 'root';
$password = '0mBe*Y3TS@Qf0cLn$';
$dbname = 'atujato_focus';


$cone = new PDO("mysql:host=$host;dbname=$dbname", "$user", "$password");
$cone->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$fetchDepartments = $cone->prepare("SELECT * FROM ub_departamento WHERE estado ='1' ORDER BY departamento");
$fetchProvinces = $cone->prepare("SELECT * FROM ub_provincia WHERE estado ='1' ORDER BY provincia");
$fetchDistricts = $cone->prepare("SELECT * FROM ub_distrito WHERE estado ='1' ORDER BY distrito");
$fetchDepartments->execute();
$i = 0;
$DepartmentsJSON = [];
while ($fetchDepartment = $fetchDepartments->fetch(PDO::FETCH_OBJ)) {
    $DepartmentsJSON[$i] = $fetchDepartment;
    $i++;
}

$fetchProvinces->execute();
$j = 0;
$ProvincesJSON = [];
while ($fetchProvince = $fetchProvinces->fetch(PDO::FETCH_OBJ)) {
    $ProvincesJSON[$j] = $fetchProvince;
    $j++;
}

$k = 0;
$fetchDistricts->execute();
$DistrictsJSON = [];
while ($fetchDistrict = $fetchDistricts->fetch(PDO::FETCH_OBJ)) {
    $DistrictsJSON[$k] = $fetchDistrict;
    $k++;
}
$response['departments'] = $DepartmentsJSON;
$response['provinces'] = $ProvincesJSON;
$response['districts'] = $DistrictsJSON;

//$resp['data'] = $response;

header("Access-Control-Allow-Origin:*");
header("Content-type: application/json");
echo json_encode($response);