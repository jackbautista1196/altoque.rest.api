<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
$data = json_decode(file_get_contents('php://input'), true);

$host = '134.209.163.214';
$user = 'root';
$password = '0mBe*Y3TS@Qf0cLn$';
$dbname = 'atujato_focus';
$name_restaurant = @$data['prm']['name_restaurant'];
$ruc_restaurant = @$data['prm']['ruc_restaurant'];
$reason_social = @$data['prm']['reason_social'];
$categorias_selected = @$data['prm']['categorias_selected'];
$category_restaurant_other = @$data['prm']['category_restaurant_other'];
$central_restaurant = @$data['prm']['central_restaurant'];
$department_select = @$data['prm']['department_select'];
$province_select = @$data['prm']['province_select'];
$district_select = @$data['prm']['district_select'];
$hasSucursal = @$data['prm']['hasSucursal'];
$direction_fiscal = @$data['prm']['direction_fiscal'];
$name_user = @$data['prm']['name_user'];
$name_user_contact = @$data['prm']['name_user_contact'];
$phone_contact = @$data['prm']['phone_contact'];
$email = @$data['prm']['email'];

try {
    $cone = new PDO("mysql:host=$host;dbname=$dbname", "$user", "$password");
    $cone->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $stmt = $cone->prepare("INSERT INTO restaurantes (  
                rest_name,
                rest_ruc, 
                rest_rsocial, 
                rest_tipo_restaurant,
                res_other_category, 
                rest_direction_central,
                idDepa, 
	            idProv,
	            idDist, 
	            rest_sucursal_delivery,
	            rest_direccion_fiscal,
	            rest_representante_legal,
		        rest_contacto_nombre,
		        rest_contacto_telefono,
		        rest_email)
            VALUES (:rest_name, :rest_ruc, :rest_rsocial, :rest_tipo_restaurant, :res_other_category, :rest_direction_central,
             :idDepa, :idProv, :idDist, :rest_sucursal_delivery, :rest_direccion_fiscal, :rest_representante_legal, :rest_contacto_nombre, :rest_contacto_telefono, :rest_email);
            ");
    $stmt->bindParam(':rest_name', $name_restaurant);
    $stmt->bindParam(':rest_ruc', $ruc_restaurant);
    $stmt->bindParam(':rest_rsocial', $reason_social);
    $stmt->bindParam(':rest_tipo_restaurant', $categorias_selected);
    $stmt->bindParam(':res_other_category', $category_restaurant_other);
    $stmt->bindParam(':rest_direction_central', $central_restaurant);
    $stmt->bindParam(':idDepa', $department_select);
    $stmt->bindParam(':idProv', $province_select);
    $stmt->bindParam(':idDist', $district_select);
    $stmt->bindParam(':rest_sucursal_delivery', $hasSucursal);
    $stmt->bindParam(':rest_direccion_fiscal', $direction_fiscal);
    $stmt->bindParam(':rest_representante_legal', $name_user);
    $stmt->bindParam(':rest_contacto_nombre', $name_user_contact);
    $stmt->bindParam(':rest_contacto_telefono', $phone_contact);
    $stmt->bindParam(':rest_email', $email);

    if ($stmt->execute()) {
        $response['val'] = 1;
        $response['msg'] = 'En las siguientes 48 horas, uno de nuestros representantes se
contactará contigo para validar la información enviada y
continuar con el proceso de registro de tu restaurante
ingresando tu carta y zona de reparto. ' . "\n" . 'Muchas gracias.' . "\n" . 'Altoque.Rest';
    } else {
        $response['val'] = 0;
        $response['msg'] = 'Ocurrió un error vuelva a intentarlo';
    }

    echo json_encode($response);
} catch (PDOException $Err) {
    return $Err->getMessage();
}